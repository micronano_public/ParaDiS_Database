########################################################################
# This Python script plots correlation matrix plot between discrete dislocaiton dynamics
# simulation features (N_i, rho_i, S_i, dotGamma_i) where i=1:12 represent the slip systems in FCC
# The matrix plots are presented in figure (3) of the following paper:
# Sh. Akhondzadeh et. al., Slip-Free multiplication and complexity of
# dislocation networks in FCC metals, Materials Theory (2020).
# hamedakh@stanford.edu

# Ni: Number of links per unit volume on each slip system
# Rhoi: Dislocaiton density on each slip systems
# dotGammai: plastic shear rate on individual slip systems.
# Si: Schmid factor of slip system i
########################################################################

# Import packages
import numpy as np
import numpy.matlib as mb
from mpl_toolkits.mplot3d import Axes3D
from scipy.io import loadmat, savemat
import matplotlib.pyplot as plt
import sys, os

# This function draws a think square around the coplanar slip systems in the correlation matrix plot
def highlight_cell(x,y, ax=None, **kwargs):
    rect = plt.Rectangle((x-1.5, y-1.5), 3,3, fill=False, **kwargs)
    ax = ax or plt.gca()
    ax.add_patch(rect)
    return rect

FigFolder = 'Figures';

cnFreq = 100
YIELDstrain = 0.05e-2


# Load DDD simulation data
# each simulation data is cut into averaging windows (which is also refered to as a chunk) as explained in the paper.
data_chunk = loadmat('DDD_120sims_nWindows9_1e3.mat')['DDD_120sims_avgWindows']
data = loadmat('DDD_120sims_1e3.mat')['DDD_120sims']


nChunks = data_chunk['Strain'][0,10].shape[1]
nSimul = data['SchmidFactor'].shape[1]
nData = nSimul * nChunks


labels_rho = [ r"$\rho_1$", r"$\rho_2$", r"$\rho_3$", r"$\rho_4$",
           r"$\rho_5$", r"$\rho_6$", r"$\rho_7$", r"$\rho_8$",
           r"$\rho_9$", r"$\rho_{10}$", r"$\rho_{11}$", r"$\rho_{12}$"]

labels_N = [ r"$N_1$", r"$N_2$", r"$N_3$", r"$N_4$",
           r"$N_5$", r"$N_6$", r"$N_7$", r"$N_8$",
           r"$N_9$", r"$N_{10}$", r"$N_{11}$", r"$N_{12}$"]

labels_systems = [ "A2", "A3", "A6", "B2", "B4", "B5", "C1", "C3", "C5", "D1", "D4", "D6" ]

labels_dG = [ r"$\dot{\gamma}_1$", r"$\dot{\gamma}_2$", r"$\dot{\gamma}_3$", r"$\dot{\gamma}_4$",
           r"$\dot{\gamma}_5$", r"$\dot{\gamma}_6$", r"$\dot{\gamma}_7$", r"$\dot{\gamma}_8$",
           r"$\dot{\gamma}_9$", r"$\dot{\gamma}_{10}$", r"$\dot{\gamma}_{11}$", r"$\dot{\gamma}_{12}$"]

labels_Si = [ r"$S_1$", r"$S_2$", r"$S_3$", r"$S_4$",
              r"$S_5$", r"$S_6$", r"$S_7$", r"$S_8$",
              r"$S_9$", r"$S_{10}$", r"$S_{11}$", r"$S_{12}$"]

avgGammaidot = np.zeros((nSimul, nChunks, 12))
rhoi_DDD = np.zeros((nSimul, nChunks, 12))
Ni_DDD = np.zeros((nSimul, nChunks, 12))

Si = np.zeros((nSimul, 12))
HRate_DDD = np.zeros((nSimul,1))
for simID in range(nSimul):
    avgGammaidot[simID,:,:] = data_chunk['avgGammaidot'][0,simID]
    rhoi_DDD[simID,:,:] = data_chunk['rhoiFreq100'][0,simID]
    Ni_DDD[simID,:,:] = data_chunk['Ni'][0,simID]
    Si[simID,:] = data_chunk['SchmidFactor'][0,simID]

    yieldStep = np.argmin(abs(data['axialStrainFreq100'][0,simID] - YIELDstrain))
    maxSch =  data['SchmidFactor'][0,simID].reshape(-1,12)[0,1]
    HRate_DDD[simID] = np.polyfit( data['axialStrainFreq100'][0,simID][yieldStep:-1,0] /maxSch, data['axialStressFreq100'][0,simID][yieldStep:-1] *maxSch , 1)[0]


LH_IDs,_ = np.where(HRate_DDD < 100e6)
HH_IDs,_ = np.where(HRate_DDD >= 100e6)

#Plotting the correlation matrix for rho_i
rho_correlation  = np.zeros((12,12),dtype=float)
for i in range(12):
    for j in range(12):
        A = np.corrcoef(rhoi_DDD[:,:,i].reshape(-1).astype(float), rhoi_DDD[:,:,j].reshape(-1).astype(float))
        rho_correlation[i,j] = A[0,1]

plt.rcParams['mathtext.fontset'] = 'stix'
plt.rcParams['font.family'] = 'STIXGeneral'

rho_correlation[2,1]
plt.imshow(rho_correlation, interpolation='none')
plt.xticks(np.arange(12), labels_systems, rotation='vertical',fontsize=11)
plt.yticks(np.arange(12), labels_systems, rotation='horizontal',fontsize=11)
plt.title(r'$\rho_i$', fontsize=17 )
plt.plot(0,0,1,1,linewidth=3)
highlight_cell(1,1, color="black", linewidth=4 )
highlight_cell(4,4, color="black", linewidth=4 )
highlight_cell(7,7, color="black", linewidth=4 )
highlight_cell(10,10, color="black", linewidth=4 )
#plt.clim(-0.5,1.0)
plt.colorbar()
plt.savefig( FigFolder+'/rhoi_corr.eps',format='eps')
plt.show()





#Plotting the correlation matrix for N_i
Ni_correlation  = np.zeros((12,12),dtype=float)
for i in range(12):
    for j in range(12):
        A = np.corrcoef(Ni_DDD[:,:,i].reshape(-1).astype(float), Ni_DDD[:,:,j].reshape(-1).astype(float))
        Ni_correlation[i,j] = A[0,1]

Ni_correlation[2,1]
plt.imshow(Ni_correlation, interpolation='none')
plt.xticks(np.arange(12), labels_systems, rotation='vertical',fontsize=11)
plt.yticks(np.arange(12), labels_systems, rotation='horizontal',fontsize=11)
plt.plot(0,0,1,1,linewidth=3)
plt.title(r'$N_i$', fontsize=17 )
highlight_cell(1,1, color="black", linewidth=4 )
highlight_cell(4,4, color="black", linewidth=4 )
highlight_cell(7,7, color="black", linewidth=4 )
highlight_cell(10,10, color="black", linewidth=4 )
#plt.clim(-0.5,1.0)
plt.colorbar()
plt.savefig( FigFolder+'/Ni_corr.eps',format='eps')
plt.show()


#Plotting the correlation matrix between N_i  and Rhoi
Ni_Rhoi_correlation  = np.zeros((12,12),dtype=float)
for i in range(12):
    for j in range(12):
        A = np.corrcoef( ( Ni_DDD[:,:,i] ).reshape(-1).astype(float), ( rhoi_DDD[:,:,j] ).reshape(-1).astype(float))
        Ni_Rhoi_correlation[i,j] = A[0,1]


plt.imshow(Ni_Rhoi_correlation, interpolation='none')
plt.xticks(np.arange(12), labels_N, rotation='vertical',fontsize=16)
plt.yticks(np.arange(12), labels_rho, rotation='horizontal',fontsize=16)
plt.plot(0,0,1,1,linewidth=3)
#plt.clim(-0.5,1.0)
plt.colorbar()
plt.title(r'$(\rho_i, N_i)$', fontsize=17)
plt.savefig( FigFolder+'/Ni_Rhoi_corr.png')
plt.show()




#Plotting the correlation matrix for dotGamma_i
dGammai_correlation  = np.zeros((12,12),dtype=float)
for i in range(12):
    for j in range(12):
        A = np.corrcoef(avgGammaidot[:,:,i].reshape(-1).astype(float), avgGammaidot[:,:,j].reshape(-1).astype(float))
        dGammai_correlation[i,j] = A[0,1]

dGammai_correlation[6,7]
plt.imshow(dGammai_correlation, interpolation='nearest')
plt.xticks(np.arange(12), labels_systems, rotation='vertical',fontsize=11)
plt.yticks(np.arange(12), labels_systems, rotation='horizontal',fontsize=11)
highlight_cell(1,1, color="black", linewidth=4 )
highlight_cell(4,4, color="black", linewidth=4 )
highlight_cell(7,7, color="black", linewidth=4 )
highlight_cell(10,10, color="black", linewidth=4 )
plt.plot(0,0,1,1,linewidth=3)
plt.title(r'$\dot{\gamma}_i$', fontsize=17 )
plt.clim(-0.5,1.0)
plt.colorbar()
#plt.savefig( FigFolder+'/dotGammai_corr.png')
plt.savefig( FigFolder+'/dotGammai_corr.png',format='eps')
plt.show()



#Plotting the correlation matrix for S_i
Si_correlation  = np.zeros((12,12),dtype=float)
for i in range(12):
    for j in range(12):
        A = np.corrcoef(Si[:,i].reshape(-1).astype(float), Si[:,j].reshape(-1).astype(float))
        Si_correlation[i,j] = A[0,1]


plt.imshow(Si_correlation, interpolation='none')
plt.xticks(np.arange(12), labels_systems, rotation='vertical',fontsize=11)
plt.yticks(np.arange(12), labels_systems, rotation='horizontal',fontsize=11)
highlight_cell(1,1, color="black", linewidth=4 )
highlight_cell(4,4, color="black", linewidth=4 )
highlight_cell(7,7, color="black", linewidth=4 )
highlight_cell(10,10, color="black", linewidth=4 )
plt.title('$S_i$', fontsize=17)
plt.plot(0,0,1,1,linewidth=3)
plt.clim(-0.5,1.0)
plt.colorbar()
plt.savefig( FigFolder+'/Si_corr.eps',format='eps')
plt.show()


#Plotting the correlation matrix between S_i and dotGamma_i
Si_dotGammai_correlation  = np.zeros((12,12),dtype=float)
for i in range(12):
    for j in range(12):
        A = np.corrcoef(avgGammaidot[:,0,j].reshape(-1).astype(float), Si[:,i].reshape(-1).astype(float))
        Si_dotGammai_correlation[i,j] = A[0,1]

plt.imshow(Si_dotGammai_correlation, interpolation='nearest')

#plt.xticks(np.arange(12), labels, rotation='vertical')
plt.xticks(np.arange(12), labels_Si, rotation='vertical',fontsize=14)
plt.yticks(np.arange(12), labels_dG, rotation='horizontal',fontsize=14)
plt.title('$(S_i, \dot{\gamma}_i)$', fontsize=17)
plt.plot(0,0,1,1,linewidth=3)
plt.clim(-0.5,1.0)
plt.colorbar()
plt.savefig( FigFolder+'/Si_dotGammai_correlation.png')
plt.show()
