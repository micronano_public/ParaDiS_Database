%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% This Matlab script plots figures (4) and 5(b) of the following paper:
% Sh. Akhondzadeh et. al., Slip-Free multiplication and complexity of
% dislocation networks in FCC metals, Materials Theory (2020). 
% hamedakh@stanford.edu

% NetSi = Si-0.5Si_max is defiend according to the rule of thumb presented in the paper

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
clear;clc; close all;
    
load('DDD_120sims_1e3.mat')
obj_array = DDD_120sims;
    

eRate = 1e3;            % all the simulations in this .mat file have been run using eRate=1e3. 
fs = 28;
FigFolder = 'Figures';
mkdir(FigFolder)
saveFig = 0;


dgammadt_order = 3 ;    % Order of the polynomial fitted to the gamma_d - time curve
Rhot_order = 3;
Nt_order = 3 ;

cnFreq = 100;                                        % This a ParaDiS simulation parameter which indicates the frequency with which the restart files (microstructure) is saved
simVol = (15e-6)^3;                                  % simulation volume. We used 15 um box size
nData=size(obj_array,2);
target_gammaiS = 1e-2*[0.5];                         % In paper, we presented results at gamma_d = 0.5(%).    
gammaID = 1;    % Index of target_gammaiS for which the figures are plotted.
nGammas = length(target_gammaiS);

Gammaidot_gammaiHalf = zeros(nData, 12,nGammas);     % Slip system Gammaidot at target_gammai 
Rhoi_gammaiHalf = zeros(nData, 13,nGammas);          % Slip system dislocation density at target_gammai  
Ni_gammaiHalf = zeros(nData, 12,nGammas);            % Slip system number of links at target_gammai  

avgTaui_gammaiHalf = zeros(nData, 12,nGammas); 
slipFree_sum = zeros(nData ,nGammas);                %This does not depend on slip system, but only to simID and gammaID
slipFree_ratio = zeros(nData ,nGammas);              % Fraction of dislocation density due on slip systems exhibiting slip-free multiplication
    
Si = zeros(nData, 12); 

warning off

for gamma_id = 1:nGammas
    disp(strcat('Processing gamma_d = ',num2str( target_gammaiS(gamma_id) ) ) );
    target_gammai = target_gammaiS(gamma_id);
    for simID = 1:nData,
        NetSi  = zeros(12,1);       

        simLength = length( obj_array(simID).axialStressFreq1 );

        Si(simID,:) = obj_array(simID).SchmidFactor ;


        for sysID=1:12
           simLength100 =  length( obj_array(simID).densityPerSystemFreq100(:,sysID) );  % this is simLength/100 (100 is the cnFreq)

           % First a polynomal is fitted to the simulation values; then the polynomial is evaluated at target_gammai
           P = polyfit(obj_array(simID).totalTime, obj_array(simID).Gammai(:,sysID) , dgammadt_order);
           P_der = polyder(P);
           maxSch = max(obj_array(simID).SchmidFactor); 
           Gammaidot_gammaiHalf(simID,sysID,gamma_id) = polyval(P_der, target_gammai.*maxSch/eRate );  

           P_rhoi = polyfit(obj_array(simID).totalTime(1:100:100*simLength100), obj_array(simID).densityPerSystemFreq100(1:simLength100,sysID) , Rhot_order);
           Rhoi_gammaiHalf(simID,sysID,gamma_id) = polyval(P_rhoi, target_gammai.*maxSch/eRate );  
           
           P_Ni = polyfit(obj_array(simID).totalTime(1:100:100*simLength100), obj_array(simID).numberOfLinksFreq100(1:simLength100,sysID) , Nt_order);
           Ni_gammaiHalf(simID,sysID,gamma_id) = polyval(P_Ni, target_gammai.*maxSch/eRate ); 

           P_Taui = polyfit(obj_array(simID).totalTime, obj_array(simID).axialStressFreq1 , dgammadt_order);
           avgTaui_gammaiHalf(simID,sysID,gamma_id) = polyval(P_Taui, target_gammai.*maxSch/eRate )*obj_array(simID).SchmidFactor(sysID) ;    


           planeID = ceil(sysID/3)-1 ;
           NetSi(sysID) = obj_array(simID).SchmidFactor(sysID) - ...
                                    0.5*max( obj_array(simID).SchmidFactor(3*planeID+1), max( obj_array(simID).SchmidFactor(3*planeID+2), obj_array(simID).SchmidFactor(3*planeID+3) ) );

           if NetSi(sysID)< -0.1                % According to the rule of thumb presented in the paper, this is the condition for slip-free multiplication  
                slipFree_sum(simID, gamma_id) = slipFree_sum(simID, gamma_id) + ...
                                    1* Rhoi_gammaiHalf(simID,sysID,gamma_id) ;
           end

        end

        sysID = 13;         % sysID = 13 denotes junctions, i.e., any dislocation whose Burgers vector and normal plane does not belong to one of the 12 slip systems.
        P_rhoi = polyfit(obj_array(simID).totalTime(1:100:100*simLength100), obj_array(simID).densityPerSystemFreq100(1:simLength100,sysID) , dgammadt_order);
        Rhoi_gammaiHalf(simID,sysID,gamma_id) = polyval(P_rhoi, target_gammai.*maxSch/eRate );

        % In calculating the sli-free multiplcation density fraction, junction density is also accounted for.  
        slipFree_ratio(simID, gamma_id) = slipFree_sum(simID, gamma_id) ./ sum( Rhoi_gammaiHalf(simID,:,gamma_id) ) ;

    end
end


%%
% Sorting values of 3 planar slip systems. Each vector of 12 slip system
% values are divided into 4 (slip planes) by 3 (slip systems) where 3
% values are sorted from lowest to highest. It can be shown that the slip system with the
% lowest Schmid factor on each slip plane, correspond to Si-0.5*Simax<0 . 

dGammai_per_plane = zeros(4*nData,3,nGammas);
Rhoi_per_plane = zeros(4*nData,3,nGammas);
Ni_per_plane = zeros(4*nData,3,nGammas);

Si_per_plane = zeros(4*nData,3);
Tau_per_plane = zeros(4*nData,3,nGammas);
slipFree_per_plane = zeros(4*nData,nGammas);


for gammaID=1:nGammas
    count =0;
    for simID=1:nData
        for sysID=1:3:12        % Only one slip system on each plane is needed
            count = count+1;
            planeID= ceil(sysID/3);

            plane_Taui = [ avgTaui_gammaiHalf(simID,3*planeID-2,gammaID), ...
                           avgTaui_gammaiHalf(simID,3*planeID-1,gammaID), ...
                           avgTaui_gammaiHalf(simID,3*planeID  ,gammaID)];

            plane_Si = [obj_array(simID).SchmidFactor(3*planeID-2), ...
                        obj_array(simID).SchmidFactor(3*planeID-1), ...
                        obj_array(simID).SchmidFactor(3*planeID  )] ;

            plane_Rhoi = [ Rhoi_gammaiHalf(simID,3*planeID-2,gammaID), ...
                           Rhoi_gammaiHalf(simID,3*planeID-1,gammaID), ...
                           Rhoi_gammaiHalf(simID,3*planeID  ,gammaID) ] ;

            plane_dGammai = [ Gammaidot_gammaiHalf(simID,3*planeID-2,gammaID), ...
                              Gammaidot_gammaiHalf(simID,3*planeID-1,gammaID), ...
                              Gammaidot_gammaiHalf(simID,3*planeID  ,gammaID) ] ;


            plane_Ni = [ Ni_gammaiHalf(simID,3*planeID-2,gammaID), ...
                         Ni_gammaiHalf(simID,3*planeID-1,gammaID), ...
                         Ni_gammaiHalf(simID,3*planeID  ,gammaID) ] ;


            plane_slipFree =  ( plane_dGammai(1)* plane_Rhoi(2) +  plane_dGammai(2)* plane_Rhoi(1) ) / ...
                                            (  sqrt(plane_Rhoi(1) ) + sqrt(plane_Rhoi(2) )  ) + ...
                              ( plane_dGammai(1)* plane_Rhoi(3) +  plane_dGammai(3)* plane_Rhoi(1) ) / ...
                                            (  sqrt(plane_Rhoi(1) ) + sqrt(plane_Rhoi(3) )  ) + ...
                              ( plane_dGammai(3)* plane_Rhoi(2) +  plane_dGammai(2)* plane_Rhoi(3) ) / ...
                                            (  sqrt(plane_Rhoi(3) ) + sqrt(plane_Rhoi(2) )  ) ;              

            [Si_sorted, orders] = sort( plane_Si );


            Si_per_plane(count,:) = Si_sorted ;
            Rhoi_per_plane(count,:,gammaID) =  plane_Rhoi(orders)  ;
            Ni_per_plane(count,:,gammaID) =  plane_Ni(orders) ;
            dGammai_per_plane(count,:,gammaID) =  plane_dGammai(orders) ;
            Tau_per_plane(count,:,gammaID) =  plane_Taui(orders) ;
            slipFree_per_plane(count,gammaID) = plane_slipFree ; 

        end
    end
end


%% Plot 3D: Si -  maxP(Si) - Ni at  0.5%
figure(1)

%Plot the 3D main data points
SC1 = scatter3( Si_per_plane(:,1), Si_per_plane(:,3), Ni_per_plane(:,1,gammaID)/simVol ,'ro', ...
        'DisplayName','$S_i < 0.5\times \left( \rm{max \ \ planar \ \ S_i} \right)$' );
hold on
plot3( [ Si_per_plane(:,2); Si_per_plane(:,3)], ...
       [ Si_per_plane(:,3); Si_per_plane(:,3)], ...
       [ Ni_per_plane(:,2,gammaID)/simVol; Ni_per_plane(:,3,gammaID)/simVol] ,'bo', 'DisplayName','$S_i > 0.5\times \left( \rm{max \ \ planar \ \ S_i} \right)$');

%Adding legend
annotation('textbox',[0.18 0.68, 0.26,0.12],'string','$S_i < 0.5S_{i^{\max}}$', 'Interpreter', 'latex',...
      'FontSize',fs,'EdgeColor',[0 0 0],'Color','r','BackgroundColor',[1 1 1], 'HorizontalAlignment','center');

annotation('textarrow',[0.307 0.27],[0.68 0.1],'string','$S_i > 0.5S_{i^{\max}}$', 'Interpreter', 'latex',...
      'HeadStyle','none','LineStyle', 'none' ,'FontSize',fs, 'Color','b');


% Adding data points on the bottom plane
bottomSurface_Z  = -1e17;
scatter3( Si_per_plane(:,1), Si_per_plane(:,3), bottomSurface_Z*ones(size(Si_per_plane(:,1))) , ...
                    20, '+r','MarkerEdgeAlpha', 0.4);

scatter3( [ Si_per_plane(:,2); Si_per_plane(:,3)], ...
       [ Si_per_plane(:,3); Si_per_plane(:,3)], ...
       bottomSurface_Z*ones(size([ Si_per_plane(:,2); Si_per_plane(:,3)])), ...
                    20 ,'+b','MarkerEdgeAlpha', 0.4);


%Adding colored surface on the bottom plane  
tri = delaunayTriangulation(Si_per_plane(:,1),Si_per_plane(:,3));
triPoints = tri.Points;

TS = trisurf( tri.ConnectivityList, triPoints(:,1), triPoints(:,2),...
               bottomSurface_Z*ones(length(triPoints),1),'FaceAlpha', 0.05 ,'EdgeColor','none','FaceColor','r' );

tri = delaunayTriangulation([ Si_per_plane(:,2); 0.25],[ Si_per_plane(:,3); 0.5]);
triPoints = tri.Points;
trisurf( tri.ConnectivityList, triPoints(:,1), triPoints(:,2),...
               bottomSurface_Z*ones(length(triPoints),1),'EdgeColor','none','FaceColor','b','FaceAlpha', 0.05)
Boundary = line([0,0.25],[0,0.5], bottomSurface_Z*[1,1],'LineWidth',4,'Color','k');
uistack(Boundary,'top')

grid on
xlabel('$S_i$','Interpreter', 'latex');
h=annotation('textarrow',[0.21 0.4],[0.11 0.4],'string','$S_{i^{\max}}$', 'Interpreter', 'latex',...
      'HeadStyle','none','LineStyle', 'none', 'TextRotation',-30,'FontSize',fs+4);
  
zlabel('$\hat{N}_i \, (\rm{m^{-3}})$','Interpreter', 'latex');
box on
view([-24,13])
set(gca,'FontSize',fs);
set(gcf,'Position',[421    40   779   665])
zlim([bottomSurface_Z, 4e17])
zticks(1e17*[0,2,4,6]);
set(gca,'linewidth',1.1)

%Adding inset
axes('Position',[.63 .27 .25 .25])
plot( Si_per_plane(:,1)-0.5*Si_per_plane(:,3), Ni_per_plane(:,1,gammaID)/simVol ,'ro','DisplayName','$S_i > 0.5\times \left( \rm{max \ \ planar \ \ S_i} \right)$');
hold on
plot( Si_per_plane(:,2)-0.5*Si_per_plane(:,3), Ni_per_plane(:,2,gammaID)/simVol ,'bo', 'DisplayName','$S_i < 0.5\times \left( \rm{max \ \ planar \ \ S_i} \right)$');
plot( Si_per_plane(:,3)-0.5*Si_per_plane(:,3), Ni_per_plane(:,3,gammaID)/simVol ,'bo', 'DisplayName','$S_i < 0.5\times \left( \rm{max \ \ planar \ \ S_i} \right)$');
plot( [-0.1 -0.1], 1e17*[0.3 ,2.4], 'k', 'LineWidth',4)
plot( [ 0.1  0.1], 1e17*[0.3 ,2.4], 'k', 'LineWidth',4)

xlabel('$S_i - 0.5 S_{i^{\max}}$','Interpreter', 'latex');
grid on
box on
set(gca,'FontSize',24);
set(gca,'linewidth',2)
xticks([-0.2,-0.1, 0, 0.1, 0.2]);

if saveFig
    saveas(gca, strcat(FigFolder,'/plot3_Ni_Si.jpg') )
end


%% Plot 3D: Si -  maxP(Si) - Rhoi at  0.5%
figure(2)

%Plot the 3D main data points
SC1 = scatter3( Si_per_plane(:,1), Si_per_plane(:,3), Rhoi_per_plane(:,1,gammaID) ,'ro', ...
        'DisplayName','$S_i < 0.5\times \left( \rm{max \ \ planar \ \ S_i} \right)$' );
hold on
plot3( [ Si_per_plane(:,2); Si_per_plane(:,3)], ...
       [ Si_per_plane(:,3); Si_per_plane(:,3)], ...
       [ Rhoi_per_plane(:,2,gammaID); Rhoi_per_plane(:,3,gammaID)] ,'bo', 'DisplayName','$S_i > 0.5\times \left( \rm{max \ \ planar \ \ S_i} \right)$');


%Adding legend
annotation('textbox',[0.18 0.68, 0.26,0.12],'string','$S_i < 0.5S_{i^{\max}}$', 'Interpreter', 'latex',...
      'FontSize',fs,'EdgeColor',[0 0 0],'Color','r','BackgroundColor',[1 1 1], 'HorizontalAlignment','center');

annotation('textarrow',[0.309 0.27],[0.68 0.1],'string','$S_i > 0.5S_{i^{\max}}$', 'Interpreter', 'latex',...
      'HeadStyle','none','LineStyle', 'none' ,'FontSize',fs, 'Color','b');


% Adding data points on the bottom plane
bottomSurface_Z  = -1e11;
scatter3( Si_per_plane(:,1), Si_per_plane(:,3), bottomSurface_Z*ones(size(Si_per_plane(:,1))) , ...
                    20, '+r','MarkerEdgeAlpha', 0.4);

scatter3( [ Si_per_plane(:,2); Si_per_plane(:,3)], ...
       [ Si_per_plane(:,3); Si_per_plane(:,3)], ...
       bottomSurface_Z*ones(size([ Si_per_plane(:,2); Si_per_plane(:,3)])) , ...
                    20, '+b' ,'MarkerEdgeAlpha', 0.4);


%Adding colored surfaces on the bottom plane   
tri = delaunayTriangulation(Si_per_plane(:,1),Si_per_plane(:,3));
triPoints = tri.Points;

TS = trisurf( tri.ConnectivityList, triPoints(:,1), triPoints(:,2),...
               bottomSurface_Z*ones(length(triPoints),1),'FaceAlpha', 0.05 ,'EdgeColor','none','FaceColor','r' )

tri = delaunayTriangulation([ Si_per_plane(:,2); 0.25],[ Si_per_plane(:,3); 0.5]);
triPoints = tri.Points;
trisurf( tri.ConnectivityList, triPoints(:,1), triPoints(:,2),...
               bottomSurface_Z*ones(length(triPoints),1),'EdgeColor','none','FaceColor','b','FaceAlpha', 0.05)
Boundary = line([0,0.25],[0,0.5], bottomSurface_Z*[1,1],'LineWidth',4,'Color','k')
uistack(Boundary,'top')

grid on
xlabel('$S_i$','Interpreter', 'latex');
%ylabel('$\max^{\rm{P}}{(S_i)}$','Interpreter', 'latex','Rotation',-30);
h=annotation('textarrow',[0.21 0.4],[0.11 0.4],'string','$S_{i^{\max}}$', 'Interpreter', 'latex',...
      'HeadStyle','none','LineStyle', 'none', 'TextRotation',-30,'FontSize',fs+4);
  
zlabel('$\hat{\rho}_i \, (\rm{m^{-2}})$','Interpreter', 'latex');
box on
view([-24,13])
set(gca,'FontSize',fs);
set(gcf,'Position',[421    40   779   665])
zticks(1e11*[0,2,4,6]);
set(gca,'linewidth',1.1)

%Adding inset
axes('Position',[.63 .27 .25 .25])
plot( Si_per_plane(:,1)-0.5*Si_per_plane(:,3), Rhoi_per_plane(:,1,gammaID) ,'ro','DisplayName','$S_i > 0.5\times \left( \rm{max \ \ planar \ \ S_i} \right)$');
hold on
plot( Si_per_plane(:,2)-0.5*Si_per_plane(:,3), Rhoi_per_plane(:,2,gammaID) ,'bo', 'DisplayName','$S_i < 0.5\times \left( \rm{max \ \ planar \ \ S_i} \right)$');
plot( Si_per_plane(:,3)-0.5*Si_per_plane(:,3), Rhoi_per_plane(:,3,gammaID) ,'bo', 'DisplayName','$S_i < 0.5\times \left( \rm{max \ \ planar \ \ S_i} \right)$');

plot( [-0.1 -0.1], 1e11*[0.1 ,2.0], 'k', 'LineWidth',4)
plot( [ 0.1  0.1], 1e11*[0.1 ,2.0], 'k', 'LineWidth',4)


xlabel('$S_i - 0.5S_{i^{\max}}$','Interpreter', 'latex');
grid on
box on
set(gca,'FontSize',24);
set(gca,'linewidth',2)
xticks([-0.2,-0.1, 0, 0.1, 0.2]);

set(gca,'ytick',[])

if saveFig
    saveas(gca, strcat(FigFolder,'/plot3_Rhoi_Si.jpg') )
end


%% Plot 3D: Si -  maxP(Si) - gammaidot at  0.5%
figure(3)

%Plot the 3D main data points
SC1 = scatter3( Si_per_plane(:,1), Si_per_plane(:,3), dGammai_per_plane(:,1,gammaID) ,'ro', ...
        'DisplayName','$S_i < 0.5\times \left( \rm{max \ \ planar \ \ S_i} \right)$' );
hold on
plot3( [ Si_per_plane(:,2); Si_per_plane(:,3)], ...
       [ Si_per_plane(:,3); Si_per_plane(:,3)], ...
       [ dGammai_per_plane(:,2,gammaID); dGammai_per_plane(:,3,gammaID)] ,'bo', 'DisplayName','$S_i > 0.5\times \left( \rm{max \ \ planar \ \ S_i} \right)$');

%Adding legend
annotation('textbox',[0.2 0.72, 0.25,0.12],'string','$S_i < 0.5S_{i^{\max}}$', 'Interpreter', 'latex',...
      'FontSize',fs,'EdgeColor',[0 0 0],'Color','r','BackgroundColor',[1 1 1],'HorizontalAlignment','center');

annotation('textarrow',[0.323 0.27],[0.72 0.1],'string','$S_i > 0.5S_{i^{\max}}$', 'Interpreter', 'latex',...
      'HeadStyle','none','LineStyle', 'none' ,'FontSize',fs, 'Color','b');


% Adding data points on the bottom plane
scatter3( Si_per_plane(:,1), Si_per_plane(:,3), bottomSurface_Z*ones(size(Si_per_plane(:,1))) , ...
                    20, '+r','MarkerEdgeAlpha', 0.4);

scatter3( [ Si_per_plane(:,2); Si_per_plane(:,3)], ...
       [ Si_per_plane(:,3); Si_per_plane(:,3)], ...
       bottomSurface_Z*ones(size([ Si_per_plane(:,2); Si_per_plane(:,3)])) , ...
                    20, '+b', 'MarkerEdgeAlpha', 0.4);


%Adding colored surface on the bottom plane  
bottomSurface_Z  = -1e3;
tri = delaunayTriangulation(Si_per_plane(:,1),Si_per_plane(:,3));
triPoints = tri.Points;

TS = trisurf( tri.ConnectivityList, triPoints(:,1), triPoints(:,2),...
               bottomSurface_Z*ones(length(triPoints),1),'FaceAlpha', 0.05 ,'EdgeColor','none','FaceColor','r' )

tri = delaunayTriangulation([ Si_per_plane(:,2); 0.25],[ Si_per_plane(:,3); 0.5]);
triPoints = tri.Points;
trisurf( tri.ConnectivityList, triPoints(:,1), triPoints(:,2),...
               bottomSurface_Z*ones(length(triPoints),1),'EdgeColor','none','FaceColor','b','FaceAlpha', 0.05)
Boundary = line([0,0.25],[0,0.5], bottomSurface_Z*[1,1],'LineWidth',4,'Color','k')
uistack(Boundary,'top')

grid on
xlabel('$S_i$','Interpreter', 'latex');
%ylabel('$\max^{\rm{P}}{(S_i)}$','Interpreter', 'latex','Rotation',-30);
h=annotation('textarrow',[0.21 0.4],[0.11 0.4],'string','$S_{i^{\max}}$', 'Interpreter', 'latex',...
      'HeadStyle','none','LineStyle', 'none', 'TextRotation',-30,'FontSize',fs+4);
  
zlabel('$\dot{\gamma}_i \,(\rm{s^{-1}}) $','Interpreter', 'latex');
box on
view([-24,13])
set(gca,'FontSize',fs);
set(gcf,'Position',[421    40   779   665])
zticks(1e2*[0,5,10,15]);
zlim([-1.01e3, 2000]); 
set(gca,'linewidth',1.1)

%Adding inset
axes('Position',[.63 .28 .25 .25])
plot( Si_per_plane(:,1)-0.5*Si_per_plane(:,3), dGammai_per_plane(:,1,gammaID) ,'ro','DisplayName','$S_i > 0.5\times \left( \rm{max \ \ planar \ \ S_i} \right)$');
hold on
plot( Si_per_plane(:,2)-0.5*Si_per_plane(:,3), dGammai_per_plane(:,2,gammaID) ,'bo', 'DisplayName','$S_i < 0.5\times \left( \rm{max \ \ planar \ \ S_i} \right)$');
plot( Si_per_plane(:,3)-0.5*Si_per_plane(:,3), dGammai_per_plane(:,3,gammaID) ,'bo', 'DisplayName','$S_i < 0.5\times \left( \rm{max \ \ planar \ \ S_i} \right)$');

xlabel('$S_i - 0.5S_{i^{\max}}$','Interpreter', 'latex');
grid on
box on
set(gca,'FontSize',24);
set(gca,'linewidth',2)
xticks([-0.2,-0.1, 0, 0.1, 0.2]);

set(gca,'ytick',[])

if saveFig
    saveas(gca, strcat(FigFolder,'/plot3_dGammai_Si.jpg') )
end


%%  Plotting the fraction of dislocation density due to slip-free multiplication 

figure(4)
hold on

[X Y] = findStProjectionPoint([ 1 2 2]);
NodeM122=[X Y];
[X Y] = findStProjectionPoint([ 1 1 1]);
NodeM111=[X Y];
[X Y] = findStProjectionPoint([ 0 1 1]);
NodeM011=[X Y];

constant = 0.02;
plot([0 NodeM111(1)]+constant,[0, NodeM111(2)]+constant,'-k');

plot([0 NodeM011(1)]+constant,[0, NodeM011(2)  ]+constant,'-k');

text( NodeM122(1)+0.01+constant, NodeM122(2)+constant, '[122]','FontSize', 25 ,  'FontName', 'Times New Roman');
text( NodeM111(1)+0.01+constant, NodeM111(2)+constant, '[111]','FontSize', 25 ,  'FontName', 'Times New Roman');
text( NodeM011(1)+0.01+constant, NodeM011(2)+constant, '[011]','FontSize', 25 ,  'FontName', 'Times New Roman');
text( 0.01+constant, -0.04+constant, '[001]','FontSize', 25 ,  'FontName', 'Times New Roman');


radius = 0.003;
Color = 'r';


for id=1:nData
    plotCylinder(radius, slipFree_ratio(id, gammaID) ,obj_array(id).stTriCoor(1)+constant,obj_array(id).stTriCoor(2)+constant,Color);   
    if strcmp(obj_array(id).name ,'111_011,X=0.1')
        corner011 = id;
    end
end



set(gca,'color','none')
set(gca,'xcolor','none')
set(gca,'xtick',[])
set(gca,'ycolor','none')
set(gca,'ytick',[])
grid off
view(12,52);

set(gca,'FontSize',24)

zlabel('$ \hat{\rho_{\rm{sf}}}/ \hat{\rho}$','interpreter','latex','FontSize',32)
set(gcf,'Position',[300 500 850 700])
if saveFig
    saveas(gca, strcat(FigFolder,'/slipFreeRatio.jpg') )
end




% This functions finds the coordinates (x,y) on the stereographic
% projection triangle, given the input of Dir as the loading orientation
function [x y] = findStProjectionPoint(Dir);

		Pole=[0 0 1];
		if size(Dir,1)==3
			Dir=Dir';
		end
			
        Dir=Dir/norm(Dir);
        EquatorialPlaneProj=(Dir-dot(Dir,Pole)*Pole);   % with Pole being the normal to plane
        cTheta = dot(EquatorialPlaneProj, [1 0 0]) / ...
                    norm(EquatorialPlaneProj) ;  % y = R*cos(Theta); 
        
        signTheta = sign( dot(EquatorialPlaneProj, [0 1 0]) / ...
                    norm(EquatorialPlaneProj) );
        sTheta = signTheta * sqrt(1-cTheta^2);

        ForR_cos = dot(Dir, [0 0 1]) ;
        ForR_sin = sqrt(1-ForR_cos^2) ;
        ForR_tan = sqrt( 1/ForR_cos^2 -1 );

        Phi = atan(ForR_tan);
        R= tan(Phi/2);
        x= R*sTheta; 
        y= R*cTheta; 
        
end
        

% plots a cylinder at coordinates (x0,y0) of the stereographic triangle
function plotCylinder(r,h,x0,y0,ColorCode);
    
    [X,Y,Z]=cylinder(r);
    X=X+x0;
    Y=Y+y0;
    Z(2,:)=h;
    
    if r == 0.003
        h2=mesh(X,Y,Z,'facecolor',ColorCode,'LineStyle','-');
    elseif r == 0.006
        h2=mesh(X,Y,Z,'facecolor',ColorCode,'LineStyle','none');
    end
    
    fill3(X(1,:),Y(1,:),Z(1,:),ColorCode);
    fill3(X(2,:),Y(2,:),Z(2,:),ColorCode);
    
     topColor=[250 250 0 ]/256;
     
     fill3(X(1,:),Y(1,:),Z(1,:), topColor);
     fill3(X(2,:),Y(2,:),Z(2,:), topColor);
    
end


