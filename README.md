# ParaDiS_Database

This repository contains results of 120 discrete dislocation dynamics (DDD) simulations obtained by the ParaDiS code with subcycling integration (Sills et al. 2018) along with its GPU implementation (Bertin et al. 2019). Each simulation were running for 6-8 days using 1CPU+1GPU (Titan V or P100) on Stanford Sherlock cluster or XSEDE (Comet-GPU & Bridges-GPU).

Simulation features at slip system level have been stored into two files: DDD_120sims_1e3.mat and DDD_120sims_nWindows9_1e3.mat. The former consist of all the values at every time step (with frequency of 100), while in the latter only time averaged values over 9 windows have been stored (see JMPS paper for explanation of the time averaging). This database contains only simulations subjected to strain rate of 1e3, using a single initial configuration loaded in uniaxial tension along 120 different orientations. The data was used in the following two papers:

Sh. Akhondzadeh, R. B. Sills, N. Bertin, W. Cai, "Dislocation density-based plasticity model from massive discrete dislocation dynamics database", JMPS, 2020.

Sh. Akhondzadeh, N. Bertin, R. B. Sills, W. Cai, "Slip-free multiplication and complexity of the dislocation network", Materials Theory, 2021.

Simulation results of strain rates of 1e2 and 1e4 can be requested by contacting caiwei@stanford.edu.

Please note that:
1) Order of slip systems are consistent with the Table 1 of slip-free multiplication paper. Examples are shown below.
2) For more information of the simulation input parameters, please see above two papers.
3) numberOfLinksFreq100 parameter shows the number of links, which has to be divided by the simulation box volume to get the number density

The database was generated and organized by Hamed Akhondzadeh <br />
Stanford University, Mechanical Engineering Department, Micro and Nano Mechanics Group


<img src="Figures/rhoi_4910.jpg"  width="250" height="250"><img src="Figures/gammai_4910.jpg"  width="250" height="250"><img src="Figures/Ni_4910.jpg"  width="250" height="250">
